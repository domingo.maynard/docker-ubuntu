
FROM ubuntu:18.04

RUN apt-get update

RUN apt-get install python3 \
    python3-pip \
    util-linux

RUN export uid=1000 gid=1000 && \
	mkdir -p /home/maynard && \
	echo "maynard:x:${uid}:${gid}:maynard:/home/maynard:/bin/bash" >> /etc/passwd && \
    	echo "maynard:x:${uid}:" >> /etc/group && \
    	echo "maynard ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/maynard && \
    	chmod 0440 /etc/sudoers.d/maynard && \
	chown ${uid}:${gid} -R /home/maynard

RUN useradd maynard
USER maynard
ENV HOME /home/maynard
WORKDIR $HOME
CMD /bin/bas